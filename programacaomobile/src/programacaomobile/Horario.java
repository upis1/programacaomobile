package programacaomobile;

import java.io.Serializable;

public class Horario implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
    transient int a; 
    static int b; 
	int segundo;
	/*
	public Horario(int hora, int minuto, int segundo) {
		setSegundo(segundo);
		setMinuto(minuto);
		setHora(hora);
	}
	*/
	
	public Horario(int segundo, int a, int b) 
    { 
        this.segundo = segundo;
        this.a = a; 
        this.b = b; 
    } 
	
	public int getHora() {
		int h;
		h = (segundo/3600);
		return h;
	}
	
	public void setHora (int hora) {
		 if (hora >= 0 && hora <= 23) {
			 segundo = segundo + (hora*3600);
		 }
	 }
	 
	public int getMinuto() {
		int m;
		m = (segundo - (getHora() * 3600)) / 60;
		return m;
	}
	
	public void setMinuto(int minuto) {
		if (minuto >= 0 && minuto <= 59) {
			segundo = segundo + (minuto*60);
		}
	}
	
	public int getSegundo() {
		int s;
		s = segundo % 60;
		return s;
	}
	
	public void setSegundo(int segundo){
		if (segundo >= 0 && segundo <= 59) {
			this.segundo = this.segundo + segundo;
			if (ehUltimoSegundo()) {
				this.segundo = 0;
			}
		}
	}
	 
	public void incrementaSegundo() {
		int s = (getSegundo() + 1);
		
		if (s == 60) {
			setSegundo(1);
			incrementaMinuto();
		}else {
			setSegundo(1);
		}
	}
		
	public void incrementaMinuto() {
		int m = getMinuto() + 1;
		
		if (m == 60) {
			setSegundo(60);
			IncrementaHora();
		}else {
			setSegundo(60);
		}
	}
	
	public void IncrementaHora() {
		if (ehUltimoSegundo()) {
			this.segundo = 0;
		}else {
			setHora (getHora() + 1);
		}
	}
	 
	public boolean ehUltimoSegundo() {

		return segundo == 86400;
	}
	
	@Override
	public String toString() {
		return this.getHora() + ":" + this.getMinuto() + ":" + this.getSegundo();
	}
	
	

}
